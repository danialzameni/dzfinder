require('./bootstrap');

$('.tenetfinder-collapse').on('click',function(){
    $('.tenetfinder').toggleClass('modal-fullscreen');
});

$('.tenetfinder-close').on('click',function(){
    $('#tenetFinder').modal('hide')
});

$('.tenetfinder-folder span').on('click',function(){
    let folder=$(this).parent();
    let foldericon=$(this).find('.bi:nth-child(1)');
    if(folder.hasClass('active')){
        folder.removeClass('active');
        foldericon.removeClass('bi-folder2-open').addClass('bi-folder');
    }else{
        folder.addClass('active');
        foldericon.removeClass('bi-folder').addClass('bi-folder2-open');
    }
    $('.tenetfinder-location').html($(this).text());
    $('.tenetfinder-folder').removeClass('superactive');
    folder.addClass('superactive');
});

$('.tenetfinder-file').on('click',function(evt){
    if(!evt.originalEvent.ctrlKey){
    $('.tenetfinder-file').removeClass('active');
    }
    $(this).addClass('active');
    let classes='';
    if($(this).hasClass('tenetfinder-file-folder')){
        classes='folder-selected';
    }else{
        classes='file-selected';
    }
    $('.tenetfinder-inner-header').removeClass('file-selected folder-selected').addClass(classes);
});

$('.btn-inmodal-upload').on('click',function(evt){
    $('.tenetfinder').addClass('tenetfinder-inmodal-1-active');
});

$(document).on('change','.tenetfinder-upload :file',function(){

    let input=$(this);
    let parent=input.parent();
    let file=this.files[0];
    let filesize=file.size;
    let filename=file.name;

    // if have error
    // parent.addClass('error');
    // setTimeout(function(){parent.removeClass('error');},1000);
    
    // when upload start
    parent.addClass('uploading');
    
    setTimeout(function(){
        // after upload done
        parent.removeClass('uploading').addClass('success');setTimeout(function(){parent.removeClass('success');$('.tenetfinder-inmodal-close').click()},1500)
        $('.tenetfinder-inner-files').append('<div class="tenetfinder-file"><div class="tenetfinder-file-thumb"><i class="bi bi-file-earmark-fill"></i></div><div class="tenetfinder-file-name" title="'+filename+'">'+filename+'</div></div>');
    },2000);

});

$('.btn-inmodal-newfolder').on('click',function(evt){
    $('.tenetfinder').addClass('tenetfinder-inmodal-2-active');
});

$('.btn-inmodal-rename').on('click',function(evt){
    $('.tenetfinder').addClass('tenetfinder-inmodal-3-active');
});

$('.btn-inmodal-delete').on('click',function(evt){
    $('.tenetfinder').addClass('tenetfinder-inmodal-4-active');
});

$('.inmodal-cancel,.tenetfinder-inmodal-close').on('click',function(evt){
    $('.tenetfinder').removeClass('tenetfinder-inmodal-active tenetfinder-inmodal-1-active tenetfinder-inmodal-2-active tenetfinder-inmodal-3-active tenetfinder-inmodal-4-active tenetfinder-inmodal-5-active tenetfinder-inmodal-6-active tenetfinder-inmodal-7-active tenetfinder-inmodal-8-active tenetfinder-inmodal-9-active');
});

$('.tenetfinder-overflow').on('click',function(evt){
    $('.tenetfinder-file').removeClass('active');
    $('.tenetfinder-inner-header').removeClass('file-selected folder-selected');
});
$('.tenetfinder-header-search input').on('input',function(){
    let value = $(this).val().toLowerCase();
    let filenum=0;
    $('.tenetfinder-file').filter(function() {
      let toggle=$(this).find('.tenetfinder-file-name').text().toLowerCase().indexOf(value) > -1;
      $(this).toggle(toggle);
      if(toggle){filenum++;}
    });
    if(filenum==0){$('.tenetfinder-inner').addClass('notfound');}else{$('.tenetfinder-inner').removeClass('notfound');}
    console.log(filenum);
});