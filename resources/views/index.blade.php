<!DOCTYPE html>
<html class="h-100" lang="fa">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>DzFinder v1.1</title>
    <link rel="stylesheet" href={{ asset('css/app.css') }}>
</head>
<body class="h-100">
    <div class="container h-100 d-flex flex-column text-center align-items-center justify-content-center">
        <div class="alert alert-success mb-2" role="alert"><h1 class="mb-0 display-1">DzFinder</h1><h4 class="mb-0">Designed by daNialz</h4></div>
        <button type="button" class="btn btn-success btn-lg" data-bs-toggle="modal" data-bs-target="#tenetFinder">Upload</button>
    </div>

    <!-- Filemanager Modal Start -->
    <div class="modal fade" id="tenetFinder" tabindex="-1" aria-labelledby="tenetFinderLabel" aria-hidden="true">
    <div class="tenetfinder modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg">
        <div class="modal-content bg-superlight">
            <div class="tenetfinder-header">
                <div class="tenetfinder-header-controll">
                <i class="bi bi-arrow-left-short"></i>
                <i class="bi bi-arrow-right-short disabled"></i>
                </div>
                <div class="tenetfinder-location text-black-50">Images</div>
                <div class="tenetfinder-header-search">
                    <i class="bi bi-search"></i>
                    <input type="text">
                </div>
                <div class="tenetfinder-header-first">            
                    <div class="tenetfinder-collapse">
                    <i class="bi bi-fullscreen-exit"></i>
                    <i class="bi bi-fullscreen"></i>
                    </div>
                    <button type="button" class="btn-close tenetfinder-close" data-dismiss="modal" aria-label="Close"></button>
                </div>
            </div>
            <div class="tenetfinder-body">
                <div class="tenetfinder-outer">
                    <div class="tenetfinder-folder">
                        <span><i class="bi bi-folder"></i><b>Files</b><i class="bi bi-arrow-right-circle-fill"></i></span>
                        <div class="tenetfinder-folders">
                            <div class="tenetfinder-folder"><span><i class="bi bi-folder"></i><b>Sample</b></span></div>
                        </div>
                    </div>
                    <div class="tenetfinder-folder active superactive">
                        <span><i class="bi bi-folder2-open"></i><b>Images</b><i class="bi bi-arrow-right-circle-fill"></i></span>
                        <div class="tenetfinder-folders">
                            <div class="tenetfinder-folder"><span><i class="bi bi-folder"></i><b>Animals</b></span></div>
                            <div class="tenetfinder-folder"><span><i class="bi bi-folder"></i><b>Cars</b></span></div>
                            <div class="tenetfinder-folder"><span><i class="bi bi-folder"></i><b>Food</b></span></div>
                        </div>
                    </div>
                    <div class="tenetfinder-folder">
                        <span><i class="bi bi-folder"></i><b>Lonely Folder</b></span>
                    </div>
                </div>
                <div class="tenetfinder-inner">
                    <div class="tenetfinder-overflow"></div>
                    <div class="tenetfinder-inner-header">
                        <button type="button" class="btn-upload btn btn-primary default-selected btn-inmodal-upload">Upload</button>
                        <button type="button" class="btn-newfolder btn btn-secondary non-selected btn-inmodal-newfolder">New Subfolder</button>
                        <button type="button" class="btn-view btn btn-secondary file-selected folder-selected">View</button>
                        <button type="button" class="btn btn-secondary file-selected">Download</button>
                        <button type="button" class="btn-rename btn btn-secondary file-selected folder-selected btn-inmodal-rename">Rename</button>
                        <button type="button" class="btn-delete btn btn-secondary file-selected folder-selected btn-inmodal-delete">Delete</button>
                    </div>
                    <div class="tenetfinder-inner-files">
                        <div class="tenetfinder-file tenetfinder-file-folder">
                            <div class="tenetfinder-file-thumb"><i class="bi bi-folder-fill"></i></div>
                            <div class="tenetfinder-file-name" title="Animals">Animals</div>
                        </div>
                        <div class="tenetfinder-file tenetfinder-file-folder">
                            <div class="tenetfinder-file-thumb"><i class="bi bi-folder-fill"></i></div>
                            <div class="tenetfinder-file-name" title="Cars">Cars</div>
                        </div>
                        <div class="tenetfinder-file tenetfinder-file-folder">
                            <div class="tenetfinder-file-thumb"><i class="bi bi-folder-fill"></i></div>
                            <div class="tenetfinder-file-name" title="Food">Food</div>
                        </div>
                        <div class="tenetfinder-file">
                            <div class="tenetfinder-file-thumb"><i class="bi bi-file-earmark-image-fill"></i></div>
                            <div class="tenetfinder-file-name" title="image.jpg">image.jpg</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tenetfinder-inmodal tenetfinder-inmodal-1">
                <div class="tenetfinder-inmodal-close"></div>
                <div class="tenetfinder-inmodal-inner">
                    <label class="tenetfinder-upload">
                    <input type="file" enctype="multipart/form-data">
                    <i class="bi bi-download"></i>
                    <span class="tenetfinder-upload-result-default">Choose a file or drag it here.</span>
                    <span class="tenetfinder-upload-result-uploading">Uploading...</span>
                    <span class="tenetfinder-upload-result-error">Error!!</span>
                    <span class="tenetfinder-upload-result-success">Uploaded :)</span>
                    </label>
                </div>
            </div>
            <div class="tenetfinder-inmodal tenetfinder-inmodal-2">
                <div class="tenetfinder-inmodal-close"></div>
                <div class="tenetfinder-inmodal-inner">
                    <p>Type the new folder name:</p>
                    <input type="text" class="form-control">
                    <div class="tenetfinder-inmodal-inner-footer">
                        <button class="btn btn-success">Ok</button>
                        <button class="btn btn-light inmodal-cancel">Cancel</button>
                    </div>
                </div>
            </div>
            <div class="tenetfinder-inmodal tenetfinder-inmodal-3">
                <div class="tenetfinder-inmodal-close"></div>
                <div class="tenetfinder-inmodal-inner">
                    <p>Enter new name:</p>
                    <input type="text" class="form-control">
                    <div class="tenetfinder-inmodal-inner-footer">
                        <button class="btn btn-success">Ok</button>
                        <button class="btn btn-light inmodal-cancel">Cancel</button>
                    </div>
                </div>
            </div>
            <div class="tenetfinder-inmodal tenetfinder-inmodal-4">
                <div class="tenetfinder-inmodal-close"></div>
                <div class="tenetfinder-inmodal-inner">
                    <p>Are you sure to delete this item?</p>
                    <div class="tenetfinder-inmodal-inner-footer">
                        <button class="btn btn-danger text-white">Yes</button>
                        <button class="btn btn-light inmodal-cancel">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>


    <!-- Filemanager Modal End -->

    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
